from urllib2 import urlopen
from urllib2 import HTTPError
from urllib2 import URLError
from bs4 import BeautifulSoup as bs

# bs = BeautifulSoup(html, 'html.parser')
# print(bs.h1)


# try:
#     html = urlopen('https://pythonscrapingthisurldoesnotexist.com')
# except HTTPError as e:
#     print(e)
# except URLError as e:
#     print 'The server could not be found!'
# else:
#     print 'It worked.'


# try:
#     badContent = bs.nonExistingTag.anotherTag
# except AttributeError as e:
#     print "Tag was not found"
# else:
#     if badContent == None:
#         print "Tag was not found"
#     else:
#         print badContent