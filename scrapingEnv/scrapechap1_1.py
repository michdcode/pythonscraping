from urllib2 import urlopen
from urllib2 import HTTPError
from urllib2 import URLError
from bs4 import BeautifulSoup 

def get_title(url):
    try: #this gets the url data 
        html = urlopen(url)
    except HTTPError as e:
        return None
    try: #this parses the url data 
        bs = BeautifulSoup(html.read(), 'html.parser')
        page_title = bs.body.h1
    except AttributeError as e:
        return None
    return page_title

title = get_title('http://www.pythonscraping.com/pages/page1.html')
if title == None:
    print 'Title could not be found'
else:
    print title